# Migração do svn para o gitlab
## Este projeto foi criado para auxilio a migração dos projetos svn para o git
### Dependências:
    - git version 2.11.0
    - git-svn version 2.11.0 (svn 1.9.5)
    - ~/svn-migration-scripts.jar
    - gitcarga.sh
    
### Passos
    - instale o git e o git-svn
    - baixe este projeto
    - copie para o rais do seu usuário o arquivo svn-migration-scripts.jar
    - crie uma pasta para migrar svn_to_git e copie o bash gitcarga.sh para a mesma.
    - execute o bash gitcarga.sh e informe o dados para importação do projeto.