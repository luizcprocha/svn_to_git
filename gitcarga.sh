#!/usr/bin/env bash
# Definir cores
GREEN='\033[0;32m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
RED="\033[0;31m"
NC='\033[0m'
# Read secret string
read_secret()
{
    stty -echo
    trap 'stty echo' EXIT
    read "$@"
    stty echo
    trap - EXIT
    echo
}
#
#
echo
printf "${YELLOW}#### MIGRAÇÃO DO SVN PARA O GITLAB ####${NC}"
echo
while : ; 
do
    echo
    read -p 'Qual seu usuário de acesso do SVN?  ' USERLDAP
#
    if [ -z $USERLDAP ];
    then
    	echo "Seu usuário deve ser informado"
      	continue
    else
    	break
    fi
done
while : ; 
do
    echo
    printf 'Qual sua senha de acesso ao SVN?  '
    read_secret PASSWLDAP
#
    if [ -z $PASSWLDAP ];
    then
        echo "Sua senha deve ser informado"
        continue
    else
        break
    fi
done
while : ; 
do
    echo "Precisamos obter URL da raiz do seu SVN! "
    read -p 'Qual a URL completa até a raiz do seu SVN ?  ' URLSVN
#
    if [ -z $URLSVN ];
    then
        echo "A URL do SVN deve ser informado!"
        continue
    else
        break
    fi
done
while : ; 
do
    echo
    read -p 'Qual o nome do projeto que você quer migrar do SVN para o GITLAB?  ' PROJSVN
#
    if [ -z $PROJSVN ];
    then
        echo "Nome do projeto deve ser informado"
        continue
    else
	printf "${RED}$URLSVN/$PROJSVN/${NC}"
	echo
	read -p 'Está correta a URL do projeto? s(sim)  ' SIM
        if [ $SIM = "s" ];
    	then
		    URLPROJSVN="$URLSVN/$PROJSVN/"
		    break
	    else
        	continue
	    fi
    fi
done
while : ; 
do
    echo
    read -p 'Cole aqui a URL .git do novo projeto no GITLAB  ' URLGIT
#
    if [ -z $URLGIT ];
    then
        echo "URL do novo projeto no GITLAB deve ser informado"
        continue
    else
        printf "${RED}$URLGIT${NC}"
        echo
        read -p 'Está correta a URL do novo projeto no GITLAB? s(sim)  ' SIM1
        if [ $SIM1 = "s" ];
        then
		    NEWURLGIT=$(echo $URLGIT | sed 's/@/'":"$PASSWLDAP"@"'/') 
            break
        else
            continue
        fi
    fi
done
echo
while : ; 
do
    echo
    read -p 'Qual o nome do branch para eleger como master? (de ENTRER se for o trunk)  ' BRANCHSVN

    if [ -z $BRANCHSVN ];
    then
        MASTERSVN="/trunk"
        break
    else
        MASTERSVN="/branches/$BRANCHSVN"
        break
    fi
done
while : ; 
do
    echo
    echo "O nome do projeto SVN pode ser criado com outro nome no GIT. "
    read -p 'Qual o nome do diretorio do projeto .git ' PATHGIT

    if [ -z $PATHGIT ];
    then
        echo "Nome do direório do projeto .git deve ser informado"
        continue
    else
        printf "${RED}$PATHGIT${NC}"
        echo
        read -p 'Está correta o nome do diretorio do projeto .git? s(sim)  ' SIM1
        if [ $SIM1 = "s" ];
        then 
            break
        else
            continue
        fi
    fi
done
echo
echo
printf "${YELLOW}1/3-Passo: Verificar assinaturas de commits dos usuários no projeto SVN${NC}"
echo
echo $PASSWLDAP | sudo -S java -jar ~/svn-migration-scripts.jar authors $URLPROJSVN $USERLDAP $PASSWLDAP > authors_$PROJSVN.txt
#
while : ; 
do
    echo "Para fazer um relacionamento dos usuarios do SVN com usuarios do GIT, "
    echo "informe só o dominio dos endereços de email dos usuarios de sua empresa! "
    echo "  Ex: nome.usuario@mycompany.com  -- SERÁ SUBISTITUIDO POR -->   nome.usuario@dominio.empresa.com.br "
    read -p 'Qual o dominio dos usuarios de email da sua empresa?  ' DOMSVNGIT
#
    if [ -z $DOMSVNGIT ];
    then
        echo "O Dominio do GIT deve ser informado!"
        continue
    else
        break
    fi
done
#
sed -i "s'mycompany.com'$DOMSVNGIT'g" authors_$PROJSVN.txt
echo
echo
printf "${YELLOW}Assinatura do projeto no SVN ${RED}$PROJSVN${NC} ${YELLOW}concluída${NC}"
echo
echo
printf "${YELLOW}2/3-Passo: Importando projeto do SVN: ${RED}$URLPROJSVN${NC}"
echo
git svn clone --no-metadata --stdlayout --authors-file=authors_$PROJSVN.txt --prefix="" $URLPROJSVN $PATHGIT
echo
echo
printf "${YELLOW}Importação do projeto ${RED}$PROJSVN${NC} ${YELLOW}concluída${NC}"
echo
echo
PROJSVNG="${PROJSVN}.git"
PROJSVNC="${PROJSVN}_temp"
mkdir $PROJSVNG
cd $PROJSVNG/
git init --bare
git symbolic-ref HEAD refs/heads/trunk
cd ..
cd $PATHGIT/
git remote add bare ../$PROJSVNG
git config remote.bare.push 'refs/remotes/*:refs/heads/*'
git push bare
cd ..
cd $PROJSVNG/
git branch -m trunk master
printf "${YELLOW}3/3-Passo: Regerando referências das tags e branchs para o git${NC}"
echo
git for-each-ref --format='%(refname)' refs/heads/tags |  # para cada branch em refs/heads/tags
cut -d / -f 4 |
while read ref
do
  git tag "$ref" "refs/heads/tags/$ref";                  # cria-se a tag a partir do branch
  git branch -D "tags/$ref";                              # remove-se a tag
done
echo
cd ..
mkdir $PROJSVNC
cd $PROJSVNC/
git clone --bare ../$PROJSVNG
cd $PROJSVNG/
git remote add novo $NEWURLGIT
git push --all novo
git push --tags novo
echo
printf "${YELLOW}#### MIGRAÇÃO DO SVN PARA O GITLAB CONCLUIDA ####${NC}"
echo
echo